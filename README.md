# workhours: console time registration utility

Workhours can be used to keep track of the time you spend at the job. If you do
your job on the computer like a programmer, sysadmin and such, when you're
logged on then you most likely do stuff for the job at the company you are
working for.


## installation

Just copy the script `workhours.py` to a directory part of the PATH environment
variable to make it execute when typed at any location.

example: `cp workhours.py /usr/local/bin/workhours`

## time registration file

This file is suffixed with the week number, like:

```bash
time_registration_week_01.csv
```

Basic contents of the time registration file:

```bash
date,start,end,total,pause,user,label
1970-01-01,07:00,15:30,08:00,00:30
```

Most are obvious though "user" and "label", are optional fields. You can use
them to alter the "total" time with an additional value. The field "user" is
the time to add to total, thus a positive value increases "total" and.. 
Note that you can use either form: 10 or 00:10.
The field "label" can be used (or ommitted) to give some kind of label and
group them. Use the "-s" option to see what it does..


## usage

To get a list of available options, type:
`workhours -h`

When invoked without any options (or -e) it will update the time registration
file located at:
`~/.workhours`

*Note: when invoked with `-e`, a predefined pause is added only once at the creation of a new
time entry within the registration file. If you would like that to happen,
add '-e' to cronjob definition*

To have an automagically updated time registration file add a user cronjob
using `crontab`, you could use the following definition:

```bash
crontab -e
# add (for example):
# */5 7-18 * * 1-5 /usr/bin/python /usr/local/bin/workhours
```


## examples

Show an *overview* of currently added entries:

```bash
workhours -s
```

Show *human readable information* for the current day:

```bash
workhours -i
```

To use "*pause mode*", which keeps track of used minutes of breaks (like lunch,
smoke break and such):

```bash
workhours -p
# type ctrl-c to quit and update registration file
```

Create an *archive file* from a already created time registration file:

```bash
cd ~/.workhours
workhours -sf time_registration_week_<week>.csv \
> time_registration_week_<week>.txt
```
